# Madplan

Meal plan template generator written in Python. Will produce a
empty meal plan template as a PDF file, which can be printed and used for
organizing meals.

Configurable by editing in the top of `madplan.py`.

## Screenshot
![alt Example Screenshot](screenshots/screen1.png)

## Dependencies
 - `python3`
 - `reportlab`

## Usage
 - Edit the configuration section in the top of `madplan.py`
 - Run `python3 madplan.py`
 - Print and hang in your kitchen

Bon Appétit!

