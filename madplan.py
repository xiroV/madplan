##### CONFIGURATION #####

# Start date
# Note: The entire week of the chosen date will be included
day = 11 
month = 6
year = 2018

# Number of sheets/pages
sheetsnum = 2

# Language ("da" or "en")
lang = "en"

# Include week numbers on top of each page
print_week_numbers = True


##### END OF CONFIGURATION #####

import datetime
from reportlab.lib.pagesizes import landscape, A4
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont

pdfmetrics.registerFont(TTFont('Quicksand', 'Quicksand/Quicksand-Regular.ttf'))
pdfmetrics.registerFont(TTFont('Raleway', 'Raleway/Raleway-Regular.ttf'))

canvas = canvas.Canvas("madplan.pdf", pagesize=landscape(A4))
datedelta = datetime.timedelta()

for n in range(sheetsnum):
    adddays = n * 21
    substractdays = datetime.date(year, month, day).weekday()
    startdate = datetime.timedelta(days=adddays) + datetime.date(year, month, day) - datetime.timedelta(days=substractdays)
    weeknumber = startdate.isocalendar()[1]
    enddate = startdate + datetime.timedelta(weeks=2)
    endweeknumber = enddate.isocalendar()[1]

    canvas.setLineWidth(.3)
    canvas.setFont('Quicksand', 18)
    
    if lang == "da":
        title_string = "Madplan"
    else:
        title_string = "Meal Plan"

    canvas.drawString(20,560,title_string)

    if lang == "da":
        week_string = "Uge " + str(weeknumber) + ' - ' + str(endweeknumber)
    else:
        week_string = "Week " + str(weeknumber) + ' - ' + str(endweeknumber)

    if print_week_numbers:
        canvas.drawRightString(820,560,week_string)

    canvas.setFont('Quicksand', 12)

    if lang == "da":
        days = ["Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "Lørdag", "Søndag"]
    else:
        days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

    day_x_start = 20 
    day_x_sep = 116
    day_x = day_x_start

    for day_name in days:
        canvas.drawString(day_x,520, day_name)
        day_x += day_x_sep

    vline_pos = (day_x_start - 10) + day_x_sep

    for line in range(0, len(days)-1):
        canvas.line(vline_pos,540,vline_pos,40)
        vline_pos += day_x_sep

    hline_pos = 510
    hline_sep = 160
    for line in range(0, 3):
        canvas.line(day_x_start,hline_pos,820,hline_pos)
        hline_pos -= hline_sep

    canvas.setFont('Raleway', 8)

    date_y_pos = 500
    for week_no in range(0,3):
        date_x_pos = day_x_start + day_x_sep - 14
        for day_no in range(0, 7):
            daynum = startdate + datetime.timedelta(days=day_no,weeks=week_no)
            canvas.drawRightString(date_x_pos, date_y_pos, str(daynum.strftime("%b")) + ' ' + str(daynum.day))
            date_x_pos += day_x_sep
        date_y_pos -= hline_sep

    canvas.showPage()    

canvas.save()
